package syntax_analyzer.syntax_tree;

import java.util.ArrayList;

public class SyntaxTreeNode {
  private final SyntaxTreeNode root;
  private final char nodeSymbol;
  private final String nodeContent;
  private ArrayList<SyntaxTreeNode> childSyntaxTreeNodes;

  public SyntaxTreeNode(SyntaxTreeNode parent, char nodeSymbol, String nodeContent) {
    this.root = parent;
    this.nodeSymbol = nodeSymbol;
    this.nodeContent = nodeContent;
    this.childSyntaxTreeNodes = new ArrayList<>();
  }

  public ArrayList<SyntaxTreeNode> getChilds() {
    return this.childSyntaxTreeNodes;
  }

  public SyntaxTreeNode addChildNode(char nodeSymbol, String nodeContent) {
    SyntaxTreeNode childSyntaxTreeNode = new SyntaxTreeNode(this, nodeSymbol, nodeContent);
    this.childSyntaxTreeNodes.add(childSyntaxTreeNode);
    return childSyntaxTreeNode;
  }

  public char getNodeSymbol() {
    return this.nodeSymbol;
  }

  public String getNodeContent() {
    return this.nodeContent;
  }
}
