package syntax_analyzer.syntax_tree;

public class SyntaxTree {
  SyntaxTreeNode root;

  public void setRoot(char nodeSymbol) {
    this.root = new SyntaxTreeNode(null, nodeSymbol, "");
  }

  public SyntaxTreeNode getRoot() {
    return this.root;
  }
}
