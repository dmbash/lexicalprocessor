package syntax_analyzer;

import lexical_analyzer.exceptions.LexicalError;
import syntax_analyzer.parsers.DescentParser;
import syntax_analyzer.syntax_tree.SyntaxTree;

public class SyntaxAnalyzer {

  private IParser parser;

  public SyntaxAnalyzer(SyntaxAnalyzerMethod method) {
    if (method == SyntaxAnalyzerMethod.DescentParser) {
      parser = new DescentParser();
    }
  }

  public SyntaxTree analyze(String line) throws LexicalError, SyntaxError, ContextError {
    System.out.println("line = " + line);
    parser.setInput(line);
    SyntaxTree syntaxTree = parser.parse();
    return syntaxTree;
  }
}
