package syntax_analyzer;

public class ContextError extends Exception {
  public ContextError(String msg) {
    super("Context error on " + msg);
  }
}
