package lexical_analyzer;

import lexical_analyzer.dfa.Machine;
import lexical_analyzer.dfa.MachineFactory;
import lexical_analyzer.exceptions.IllegalTransition;
import lexical_analyzer.exceptions.InvalidToken;
import lexical_analyzer.exceptions.LexicalError;
import lexical_analyzer.exceptions.NotFinalState;

public class Scanner {

  private String currentLine;
  private int currentRowIndex;
  private int currentColumnIndex;
  private Token currentToken;
  private String currentTokenValue;

  public Scanner() {
    resetColumnIndex();
    resetRowIndex();
  }

  public Token readNextToken() throws LexicalError {
      StringBuilder wordBuilder = new StringBuilder();
      char inputChar = getCurrentChar();
      // that skips spaces in line to another token
      while (inputChar == ' ') { // example: '___________abc_________a'
        incrementColumnIndex();
        inputChar = getCurrentChar();
      }
      // that collects all non-spaces excluding new-lines, string terminators
      while (inputChar != ' ' && inputChar != '\n' && inputChar != '\0') {
        wordBuilder.append(inputChar);
        incrementColumnIndex();
        inputChar = getCurrentChar();
      }

      if (wordBuilder.length() < 1) {
        setCurrentToken(Token.Epsilon);
        setCurrentTokenValue("");
        return Token.Epsilon;
      }

      String word = wordBuilder.toString();

      Token tokenType = null;
    try {
      tokenType = detectTokenType(word);
    } catch (InvalidToken invalidToken) {
      LexicalError lexErr = new LexicalError(getCurrentTextPosition());
      lexErr.setStackTrace(invalidToken.getStackTrace());
      throw lexErr;
    }

    try {
      processWithMachine(word, MachineFactory.getMachine(tokenType));
    } catch (IllegalTransition | NotFinalState ex) {
      LexicalError lexErr = new LexicalError(getCurrentTextPosition());
      lexErr.setStackTrace(ex.getStackTrace());
      throw lexErr;
    }

      setCurrentToken(tokenType);
      setCurrentTokenValue(word);
      if (inputChar == '\n') {
        System.out.println("linebreak");
        incrementRowIndex();
      }
      return tokenType;
  }

  // getters

  public String getTokenValue() {
    return this.currentTokenValue;
  }

  public int getCurrentRowIndex() {
    return currentRowIndex;
  }

  public int getCurrentColumnIndex() {
    return this.currentColumnIndex;
  }

  public Token getCurrentToken() {
    return this.currentToken;
  }

  private int getCurrentLineLength() {
    return this.currentLine.length();
  }

  private char getCurrentChar(){
    if (getCurrentColumnIndex() == getCurrentLineLength())
      return '\0';
    return this.currentLine.charAt(this.currentColumnIndex);
  }

  public String getCurrentTextPosition() {
    return getCurrentColumnIndex() + ":" + getCurrentRowIndex();
  }

  // setters

  public void setCurrentLine(String line) {
    this.currentLine = line;
  }

  private void setCurrentRowIndex(int currentRowIndex) {
    this.currentRowIndex = currentRowIndex;
  }

  private void setCurrentToken(Token token) {
    this.currentToken = token;
  }

  private void setCurrentTokenValue(String value) {
    this.currentTokenValue = value;
  }

  // methods

  public void resetScanner(String line) {
    setCurrentLine(line);
    resetColumnIndex();
    resetRowIndex();
  }

  public void processNextLine(String line) {
    setCurrentLine(line);
    resetColumnIndex();
    incrementRowIndex();
  }

  private Token detectTokenType(String word) throws InvalidToken {
    switch (word.charAt(0)) {
      case '1':
        return Token.FirstWord;
      case 'a':
      case 'b':
      case 'c':
      case 'd':
        return Token.SecondWord;

      case '+':
        return Token.PlusSign;

      case '*':
        return Token.AsteriskSign;

      case '/':
        return Token.Commentary;

      default:
        throw new InvalidToken();
    }
  }

  private void processWithMachine(String word, Machine machine) throws IllegalTransition, NotFinalState {
    for (int i = 0; i < word.length(); i++) {
      machine.switchState(word.charAt(i));
    }
    if (machine.canStop()) {
      return;
    } else {
      throw new NotFinalState();
    }
  }

  private void incrementColumnIndex() {
    this.currentColumnIndex += 1;
  }

  private void resetColumnIndex() {
    this.currentColumnIndex = 0;
  }

  private void incrementRowIndex() {
    this.setCurrentRowIndex(this.getCurrentRowIndex() + 1);
  }

  private void resetRowIndex() {
    this.setCurrentRowIndex(0);
  }
}
