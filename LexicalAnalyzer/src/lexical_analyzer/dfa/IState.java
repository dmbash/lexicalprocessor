package lexical_analyzer.dfa;

import lexical_analyzer.exceptions.IllegalTransition;

public interface IState {
	void addTransition(ITransition tr);
	IState makeTransition(char ch) throws IllegalTransition;
	boolean isFinal();
}
