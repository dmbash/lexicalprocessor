package lexical_analyzer.dfa;

import java.util.ArrayList;
import java.util.List;

import lexical_analyzer.exceptions.IllegalTransition;

public class State implements IState {
	
	private boolean isFinal;
	private List<ITransition> transitions;
	
	public State(boolean isFinal) {
		this.isFinal = isFinal;
		this.transitions = new ArrayList<ITransition>();
	}

	public State() {
		this(false);
	}

	@Override
	public void addTransition(ITransition tr) {
		this.transitions.add(tr);
	}

	@Override
	public IState makeTransition(char ch) throws IllegalTransition {
		for (ITransition transition : this.transitions) {
			if (transition.accept(ch)) return transition.toState();
		}
		throw new IllegalTransition();
	}

	@Override
	public boolean isFinal() {
		return this.isFinal;
	}

}
